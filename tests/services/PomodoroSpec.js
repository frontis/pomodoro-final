import Pomodoro from '../../js/services/Pomodoro.js'
import Time from '../../js/libraries/Time.js'
import Bus from '../../js/libraries/Bus.js'
import Callback from '../Callback.js'
import StubDate from '../StubDate.js'

describe('Pomodoro service', () => {
  let bus, time, date = null

  beforeEach(() => {
    bus = new Bus()
    date = new StubDate()
    time = new Time(date)
  })

  it('subscribes to start event', () => {
    spyOn(bus, 'subscribe')

    new Pomodoro(bus, time)

    expect(bus.subscribe).toHaveBeenCalledWith('timer.start_requested', aCallback())
  })

  xit('on start, publishes start event with initial time left', () => {
    const callback = new Callback(bus, 'timer.start')
    const initialTimeLeft = { isPaused: false, minutes: 25 }
    const service = new Pomodoro(bus, time)

    service.start()

    expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeft)
  })

  xit('does not send a negative time left', () => {
    const callback = new Callback(bus, 'timer.timeLeft')
    const zeroTimeLeft = { isPaused: false, minutes: 0 }
    const service = new Pomodoro(bus, time)
    service.start()
    date.moveToFuture()

    service.calculateTimeLeft()

    expect(callback.hasBeenCalledWith()).toEqual(zeroTimeLeft)
  })

  xit('after pause the count down the time left does not changes', () => {
    const pausedTimeLeft = { isPaused: true, minutes: 24 }
    const callback = new Callback(bus, 'timer.timeLeft')
    const service = new Pomodoro(bus, time)
    service.start()
    date.moveOneMinuteToFuture()
    service.calculateTimeLeft()

    service.pause()

    date.moveOneMinuteToFuture()
    service.calculateTimeLeft()
    expect(callback.hasBeenCalledWith()).toEqual(pausedTimeLeft)
  })

  xit('announces when is paused', () => {
    const pausedTimeLeft = { isPaused: true, minutes: 0 }
    const callback = new Callback(bus, 'timer.paused')
    const service = new Pomodoro(bus, time)
    date.moveOneMinuteToFuture()

    service.pause()

    expect(callback.hasBeenCalledWith()).toEqual(pausedTimeLeft)
  })

  xit('announces when is reseted', () => {
    const pausedTimeLeft = { isPaused: true, minutes: 25 }
    const callback = new Callback(bus, 'timer.reseted')
    const service = new Pomodoro(bus, time)
    service.start()
    date.moveOneMinuteToFuture()

    service.reset()

    expect(callback.hasBeenCalledWith()).toEqual(pausedTimeLeft)
  })

  xit('can resume the countdown', () => {
    const resumedTimeLeft = { isPaused: false, minutes: 23 }
    const callback = new Callback(bus, 'timer.timeLeft')
    const service = new Pomodoro(bus, time)
    service.start()
    date.moveOneMinuteToFuture()
    service.calculateTimeLeft()
    service.pause()

    service.start()

    date.moveOneMinuteToFuture()
    service.calculateTimeLeft()
    expect(callback.hasBeenCalledWith()).toEqual(resumedTimeLeft)
  })

  function aCallback() {
    return jasmine.any(Function)
  }
})
