import Timer from '../views/Timer.js'

const randomPollingFrecuence = Math.ceil(Math.random() * 10) * 200
const DEFAULT_PROPERTIES = { minutes: 0 }

class Pomodoro {
  constructor(container, bus, document) {
    this.container = container
    this.renderer = new Timer(document)
    this.bus = bus
    this.timeOut = null

    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('timer.start', this._showCountDown.bind(this))
    this.bus.subscribe('timer.timeLeft', this._showCountDown.bind(this))
    this.bus.subscribe('timer.paused', this._showCountDown.bind(this))
    this.bus.subscribe('timer.reseted', this._showCountDown.bind(this))
  }

  draw() {
    this._drawRenderer(DEFAULT_PROPERTIES)
  }

  pause() {
    this.bus.publish('timer.pause')
  }

  reset() {
    this.bus.publish('timer.reset')
  }

  _startCountDown() {
    this.bus.publish('timer.start_requested')
  }

  _showCountDown(message) {
    this._askCountDown()
    this._drawRenderer(message)
  }

  _askCountDown() {
    clearTimeout(this.timeOut)

    this.timeOut = setTimeout(() => {
      this.bus.publish('timer.askTimeLeft')
    }, randomPollingFrecuence)
  }

  _drawRenderer(message) {
    const callbacks = {
      startCountDown: this._startCountDown.bind(this),
      pause: this.pause.bind(this),
      reset: this.reset.bind(this)
    }

    this.container.innerHTML = this.renderer.render(message)
    this.renderer.addCallbacks(callbacks)
  }
}

export default Pomodoro
